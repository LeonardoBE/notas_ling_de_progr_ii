<?php

use PHPUnit\Framework\TestCase;
use Leobe\Julius\Movimentacao;


final class NovaMovimentacaoTest extends TestCase
{

    
    public function testInstanciationOfMovimentacao() {
        
        $movimentacao = new Movimentacao(111.11, '02-02-2022', $this);

        $this->assertInstanceOf(Movimentacao::class, $movimentacao);

    }

    public function testCreateWithTheSameValue() {
        $movimentacao = new Movimentacao(rand(), '02-02-2022', $this);
        $movimento = $movimentacao->criarMovimento();
        $this->assertEquals($movimentacao->valor, $movimentacao->valor);
        $this->assertEquals($movimento->origem, $movimentacao);
    }
}