<?php

use PHPUnit\Framework\TestCase;
use Leobe\Julius\Conta;
use Leobe\Julius\Movimentacao;


final class ContaTeste extends TestCase
{
    public function testInstanciationOfConta() {
        
        $contaTeste = new Conta(rand(1, 1000) / 10);

        $this->assertInstanceOf(Conta::class, $contaTeste);

    }

    public function testAddMovimento() {
        $contaTeste = new Conta(rand(1, 1000) / 10);

        $contaTeste->addMovimentacao(new Movimentacao(118.00,'10-01-2022', $this));

        $this->assertEquals($contaTeste->total(), 118.00);


    }


}
 