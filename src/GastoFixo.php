<?php
use Leobe\Julius\Movimentacao;
use Leobe\Julius\FabricaDeMovimentacao;

class GastoFixo implements FabricaDeMovimentacao{
    public function __construct(
        public $descricao,
        public $valor,
        public $dataDePagamento,
        ){

        }

        public function criarMovimento():Movimentacao{
            return new Movimentacao($this->valor, new DateTime(), $this);
        }
    

}