<?php 


class GastoAssinaturas {
    public $valoresAssinturas = [];
    

    public function __construct(array $valoresAssinaturas) {
        $this->valoresAssinaturas = $valoresAssinaturas;
    }

    public function somarGastosAssinaturas() {
        $totalGastos = 0;
        foreach($this->valoresAssinaturas as $assinatura) {
            $totalGastos += $assinatura->serviceCost;
    
        }
        
        return $totalGastos;
    }

}

