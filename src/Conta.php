<?php

namespace Leobe\Julius;
use Leobe\Julius\Movimentacao;

class Conta {
    public function __construct(
        protected $saldo = 0,
        protected $movimentos = []
    ){   
    }       

    public function addMovimentacao(Movimentacao $movimentacao) {
        $this->movimentos[] = $movimentacao;
        $this->saldo += $movimentacao->valor;
    }

    public function size() : int {
        return count($this->movimentos);
    }

    public function total() : float {
        return array_sum(array_map( function( $it ){
            return $it->valor;
        }, $this->movimentos));
    }

}

