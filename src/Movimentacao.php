<?php

namespace Leobe\Julius;
use Leobe\Julius\FabricaDeMovimentacao;

class Movimentacao implements FabricaDeMovimentacao {
    public function __construct(
        public $valor,
        public $data,
        public $origem
        ){    
        }

        public function criarMovimento():Movimentacao{
            return new Movimentacao($this->valor, new \DateTime(), $this);
        }
}








