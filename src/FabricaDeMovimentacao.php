<?php

namespace Leobe\Julius;
use Leobe\Julius\Movimentacao;


interface FabricaDeMovimentacao {
    public function criarMovimento():Movimentacao;   
}