<?php 
use Leobe\Julius\Movimentacao;
use Leobe\Julius\FabricaDeMovimentacao;


class Assinatura implements FabricaDeMovimentacao{
    public function __construct(
    public $valorDoServico,
    public $nomeDoServico,
    public $dataDeRenovacao,
    ){        
    }

    public function criarMovimento():Movimentacao{
        return new Movimentacao($this->valorDoServico, new DateTime(), $this);
    }

}





