<?php 

class Pessoa {
    public $nome;

    function __construct(string $nome) {
        $this->nome = $nome;
    }

    function __toString() : string {
        return "Pessoa { nome: $this->nome }";
    }
}

$pessoa0 = new Pessoa('Ettore');
$pessoa1 = new Pessoa('Leandro');

echo $pessoa0, ' ', $pessoa1, "\n";


