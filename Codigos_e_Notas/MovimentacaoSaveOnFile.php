<?php

// Exemplo feito pelo Professor Ettore
// Salvando Classes em arquivo local


class ContaIO
{

    public function save(Conta $conta, $output)
    {
        $rawData = $conta->toRaw();
        $jsonData = json_encode($rawData);
        file_put_contents($output, $jsonData);
    }

    public function load($input): Conta
    {
        $rawData = json_decode(file_get_contents($input), true);
        return Conta::fromRaw($rawData);
    }
}



class Conta
{

    static function fromRaw($rawData): Conta
    {
        return new Conta(
            $rawData['saldo'],
            array_map(function($obj){ return Movimentacao::fromRaw($obj); },$rawData['movimentos'])
        );
    }

    public function toRaw(): array
    {
        return [
            'saldo' => 0,
            'movimentos' => array_map(function ($obj) {
                return $obj->toRaw();
            }, $this->movimentos),
        ];
    }

    public function __construct(
        protected $saldo = 0,
        protected $movimentos = []
    ) {
    }

    public function addMovimento(Movimentacao $movimentacao)
    {
        $this->movimentos[] = $movimentacao;
        $this->saldo += $movimentacao->valor;
    }
}

class Movimentacao
{
    static function fromRaw($rawData): Movimentacao
    {
        return new Movimentacao(
            $rawData['valor'],
            $rawData['data'],
            null
        );
    }

    public function __construct(
        public $valor,
        public $data,
        public $origem
    ) {
    }

    public function movimentar($saldo, $valor)
    {
    }

    public function toRaw(): array
    {
        return (array)$this;
    }
}


class GastoFixo
{
    public function __construct(
        public $descricao,
        public $valor,
        public $dataDePagamento,
    ) {
    }
}

class Assinatura
{
    public function __construct(
        public $valorDoServico,
        public $nomeDoServico,
        public $dataDeRenovacao,
    ) {
    }

    public function criarMovimento(): Movimentacao
    {
        return new Movimentacao($this->valorDoServico, new DateTime(), $this);
    }
}

$io = new ContaIO();

$conta1 = $io->load('conta.json');

var_dump($argv);

var_dump($conta1);
exit;

$conta1->addMovimento(new Movimentacao(-60.00, new DateTime(), NULL));
var_dump($conta1);


$netflix = new Assinatura(45.00, 'Netflix', '18-04');


$conta1->addMovimento($netflix->criarMovimento());

var_dump($conta1);
